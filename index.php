<?php

require('animal.php');
require ('ape.php');
require ('frog.php');

$sheep = new Animal("shaun");

echo "Nama hewan : ".$sheep->name . "<br>"; // "shaun"
echo "Jumlah kaki : ".$sheep->legs . "<br>"; // 2
echo "shaun termasuk hewan berdarah dingin : ";
echo var_dump($sheep->cold_blooded); // false
echo "<br><br>";
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
$sungokong = new Ape("kera sakti");
echo "nama hewan : ".$sungokong->name . "<br>";
echo "jumlah kaki : ".$sungokong->legs . "<br>"; 
echo "suara : ";
echo $sungokong->yell();

echo "<br><br>";
$kodok = new Frog("buduk");
echo "nama hewan : ".$kodok->name . "<br>";
echo "jumlah kaki : ".$kodok->legs . "<br>";
echo "cara berpindah tempat : ";
echo $kodok->jump(); // "hop hop"


?>



